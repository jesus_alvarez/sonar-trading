package com.sonartrading.ws;

import java.util.List;

public class DiffOrder {

    private Integer secuence;
    private String book;

    private List<DiffOrdersPayLoad> payLoads;


    public Integer getSecuence() {
        return secuence;
    }

    public void setSecuence(Integer secuence) {
        this.secuence = secuence;
    }

    public String getBook() {
        return book;
    }

    public void setBook(String book) {
        this.book = book;
    }

    public List<DiffOrdersPayLoad> getPayLoads() {
        return payLoads;
    }

    public void setPayLoads(List<DiffOrdersPayLoad> payLoads) {
        this.payLoads = payLoads;
    }
}
