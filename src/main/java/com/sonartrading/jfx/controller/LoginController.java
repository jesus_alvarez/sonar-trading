package com.sonartrading.jfx.controller;

import com.sonartrading.configuration.ScreensConfiguration;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.paint.Color;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;

/**
 * Created by C.JALVBU on 10/13/2017.
 */
public class LoginController implements DialogController  {
    @Autowired
    private AuthenticationManager authenticationManager;
    private ScreensConfiguration screens;
    private FXMLDialog dialog;

    public void setDialog(FXMLDialog dialog) {
        this.dialog = dialog;
    }

    public LoginController(ScreensConfiguration screens) {
        this.screens = screens;
    }

    @FXML
    Label header;
    @FXML
    TextField username;
    @FXML
    TextField password;

    @FXML
    public void login() {
        Authentication authToken = new UsernamePasswordAuthenticationToken(username.getText(), password.getText());
        try {
            authToken = authenticationManager.authenticate(authToken);
            SecurityContextHolder.getContext().setAuthentication(authToken);
        } catch (AuthenticationException e) {
            header.setText("Login failure, please try again:");
            header.setTextFill(Color.DARKRED);
            return;
        }
        dialog.close();
       screens.showScreen(screens.sonarTradingScreen());
    }

    @FXML
    public void employee() {
        username.setText("employee");
        password.setText("lol");
    }

    @FXML
    public void manager() {
        username.setText("manager");
        password.setText("password");
    }
    @FXML
    public void proxySettings(ActionEvent actionEvent) {
        dialog.close();
        screens.proxyDialog().show();
    }

    public void setConnectByProxy(ActionEvent event) {
        if (event.getSource() instanceof CheckBox) {
            CheckBox chk = (CheckBox) event.getSource();
            System.out.println("Action performed on checkbox " + chk.getText());
            if (chk.getId().equals("checkBoxByProxy")){
                boolean byProxy = chk.isSelected();
                if (byProxy){
                    if (screens.getProxySettings()==null){
                        header.setText("Please set proxy settings first");
                        header.setTextFill(Color.DARKRED);
                    }
                }
                screens.setByProxy(byProxy);
            }
        }
    }
}
