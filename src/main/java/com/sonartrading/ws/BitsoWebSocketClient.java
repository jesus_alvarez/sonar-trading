package com.sonartrading.ws;

import org.codehaus.jackson.map.ObjectMapper;

import javax.websocket.*;
import java.io.IOException;
import java.net.URI;
import java.util.logging.Level;
import java.util.logging.Logger;

@ClientEndpoint
public class BitsoWebSocketClient {

    private Session session;
    private static final Logger LOGGER = Logger.getLogger(BitsoWebSocketClient.class.getName());
    private String host;

    BitsoWebSocketListener listener;

    private boolean isConnected;

    public BitsoWebSocketClient(String host, BitsoWebSocketListener listener) {
        this.host = host;
        this.listener = listener;
    }

    private boolean connectToWebSocket(){
        WebSocketContainer container = ContainerProvider.getWebSocketContainer();
        try{
            URI uri = URI.create(getHostUrl());
            container.connectToServer(this, uri);
            isConnected = true;
        }catch (DeploymentException | IOException ex ){
            LOGGER.log(Level.SEVERE, null, ex);
        }
        return isConnected;
    }

    private String getHostUrl(){
        String url = host ;
        return url;
    }


    @OnMessage
    public void onMessage(String message){
        System.out.println("WebSocket message Received!");
        System.out.println("message : " + message);
        ObjectMapper mapper = new ObjectMapper();
        if (message.contains("action")) {
            try {
                ResponseSuscriptionMessage responseSuscriptionMessage = mapper.readValue(message, ResponseSuscriptionMessage.class);
                if (listener != null)
                    listener.onSubscription(responseSuscriptionMessage);
            } catch (IOException e) {
                LOGGER.log(Level.SEVERE, null, e);
            }

        }else {
            try {
                ServerJsonMessage serverJsonMessage = mapper.readValue(message, ServerJsonMessage.class);
                if (listener!=null)
                    listener.onServerMessage(serverJsonMessage);
            } catch (IOException e) {
                LOGGER.log(Level.SEVERE, null, e);
            }

        }

    }


    @OnOpen
    public void onOpen(Session session){
        this.session = session;
        if (listener!=null)
            listener.onOpen();
    }

    @OnClose
    public void onClose(){
        isConnected =false;
    }

    public boolean connect(){
        return connectToWebSocket();
    }

    public Session getSession() {
        return session;
    }

    public boolean isConnected(){
        return isConnected;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public void subscribe(String book, String channel) {
        ClientSuscriptionMessage message = new ClientSuscriptionMessage(book, channel);
        ObjectMapper mapper = new ObjectMapper();
        try {
                this.session.getBasicRemote().sendObject(mapper.writeValueAsString(message));
        } catch (IOException | EncodeException e ) {
            LOGGER.log(Level.SEVERE, null, e);
        }
    }
}
