package com.sonartrading.screen;

import com.sonartrading.configuration.ScreensConfiguration;
import com.sonartrading.http.DiffOrderBook;
import com.sonartrading.http.Trade;
import com.sonartrading.jfx.controller.SonarTradingController;
import com.sonartrading.service.LastOrderBook;
import com.sonartrading.ws.Ask;
import com.sonartrading.ws.Bid;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;

import java.math.BigDecimal;
import java.util.logging.Logger;

/**
 * Created by C.JALVBU on 10/16/2017.
 */
public class SonarTradingScreen extends StackPane {

    SonarTradingController controller;
    private ScreensConfiguration screen;
    private TableView<Bid> bidsTableView = new TableView<Bid>();
    private TableView<Ask> asksTableView = new TableView<Ask>();
    private TableView<Trade> recenttTradesTableView = new TableView<Trade>();
    private TableView<Trade> imaginaryTradesTableView = new TableView<Trade>();
    private TableView<DiffOrderBook> bidsOrderBookTableView = new TableView<DiffOrderBook>();
    private TableView<DiffOrderBook> asksOrderBookTableView = new TableView<DiffOrderBook>();
    private TableView<LastOrderBook> maxAndMinDataTableView = new TableView<LastOrderBook>();


    public static final Logger LOGGER = Logger.getLogger(SonarTradingScreen.class.getName());

    public SonarTradingScreen(ScreensConfiguration screen, SonarTradingController controller, boolean byProxy) {
        this.controller = controller;
        this.controller.setByProxy(byProxy);
        this.screen = screen;
        this.controller.connectoToWebSOcket();
        getChildren().add(VBoxBuilder.create()
                .children(
                        createHeader(),
                        createToolbar(),
                        createTableViewsLayout()
                ).build());
    }

    private Node createTableViewsLayout() {
        HBox hbX = new HBox();
        hbX.getChildren().addAll(createOrdersTableView(), createTradesDataTable(), createOrdersBookTableView());
        hbX.setSpacing(10);
        return hbX;
    }

    private Node createOrdersTableView(){
        VBox vbX = new VBox();
        vbX.getChildren().addAll(createBidsDataTable(), createAsksDataTable());
        return vbX;
    }

    private Node createTradesDataTable(){
        VBox vbX = new VBox();
        vbX.getChildren().addAll(createRecentTradesDataTable(), createImaginaryTradesDataTable());
        return vbX;
    }

    private Node createOrdersBookTableView(){
        VBox vbX = new VBox();
        vbX.getChildren().addAll(createBidsOrderBookDataTable(), createAsksOrderBookDataTable(), createmaxAndMinsDataTable());
        return vbX;
    }

    private Node createHeader() {
        return new ImageView(getClass().getResource("/images/header.png").toString());
    }

    private Node createToolbar() {


        ToolBar toolBar = ToolBarBuilder.create()
                .items(
                        ButtonBuilder.create()
                                .text("Close")
                                .onAction(new EventHandler<ActionEvent>() {
                                    public void handle(ActionEvent actionEvent) {
                                        close();
                                    }
                                })
                                .build(),
                        createXTextView(),
                        createNTextView(),
                        createMTextView()

                )
                .build();
        return toolBar;
    }

    private void close() {
      this.screen.close();
    }


    private Node createXTextView(){
        TextField textFieldX = new TextField ();
        textFieldX.setText("20");
        Text helpText = new Text("X");
        helpText.setFont(Font.font("Verdana", FontWeight.BOLD, 18));
        helpText.setFill(Color.WHITE);
        helpText.setStroke(Color.web("#7080A0"));
        HBox hbX = new HBox();
        hbX.getChildren().addAll(helpText, textFieldX);
        hbX.setSpacing(10);

        textFieldX.textProperty().addListener((observable, oldValue, newValue) -> {
            LOGGER.info("textFieldX changed from " + oldValue + " to " + newValue);
            Integer xValue = Integer.valueOf(newValue);
            controller.setX(xValue);
        });

        return hbX;
    }

    private Node createNTextView(){
        TextField textFieldN = new TextField ();
        textFieldN.setText("3");
        Text helpText = new Text("N");
        helpText.setFont(Font.font("Verdana", FontWeight.BOLD, 18));
        helpText.setFill(Color.WHITE);
        helpText.setStroke(Color.web("#7080A0"));
        HBox hbX = new HBox();
        hbX.getChildren().addAll(helpText, textFieldN);
        hbX.setSpacing(10);

        textFieldN.textProperty().addListener((observable, oldValue, newValue) -> {
            LOGGER.info("textFieldN changed from " + oldValue + " to " + newValue);
            Integer nValue = Integer.valueOf(newValue);
            controller.setN(nValue);
        });

        return hbX;
    }

    private Node createMTextView(){
        TextField textFieldM = new TextField ();
        textFieldM.setText("3");
        Text helpText = new Text("M");
        helpText.setFont(Font.font("Verdana", FontWeight.BOLD, 18));
        helpText.setFill(Color.WHITE);
        helpText.setStroke(Color.web("#7080A0"));
        HBox hbX = new HBox();
        hbX.getChildren().addAll(helpText, textFieldM);
        hbX.setSpacing(10);

        textFieldM.textProperty().addListener((observable, oldValue, newValue) -> {
            LOGGER.info("textFieldX changed from " + oldValue + " to " + newValue);
            Integer mValue = Integer.valueOf(newValue);
            controller.setM(mValue);
        });

        return hbX;
    }


    @SuppressWarnings("unchecked")
    private Node createBidsDataTable() {
        GridPane gridPane = new GridPane();
        Text helpText = new Text("Bids");
        helpText.setFont(Font.font("Verdana", FontWeight.BOLD, 18));
        helpText.setFill(Color.WHITE);
        helpText.setStroke(Color.web("#7080A0"));
        gridPane.addRow(1, helpText);
        gridPane.addRow(2, bidsTableView);
        gridPane.setPadding(new Insets(8));
        gridPane.setStyle("-fx-background-color: gray");
        bidsTableView.setItems(controller.getBids());
        bidsTableView.getColumns().setAll(
                TableColumnBuilder.<Bid, String>create()
                        .text("Rate")
                        .cellValueFactory(new PropertyValueFactory<Bid, String>("r"))
                        .prefWidth(100)
                        .build(),
                TableColumnBuilder.<Bid, String>create()
                        .text("Amount")
                        .cellValueFactory(new PropertyValueFactory<Bid, String>("a"))
                        .prefWidth(100)
                        .build(),
                TableColumnBuilder.<Bid, String>create()
                        .text("Value")
                        .cellValueFactory(new PropertyValueFactory<Bid, String>("v"))
                        .prefWidth(100)
                        .build(),
                TableColumnBuilder.<Bid, String>create()
                        .text("Type")
                        .cellValueFactory(new PropertyValueFactory<Bid, String>("t"))
                        .prefWidth(100)
                        .build(),
                TableColumnBuilder.<Bid, String>create()
                        .text("TimeStamp")
                        .cellValueFactory(new PropertyValueFactory<Bid, String>("d"))
                        .prefWidth(100)
                        .build()

        );
        bidsTableView.setPrefHeight(250);
        return gridPane;
    }


    @SuppressWarnings("unchecked")
    private Node createAsksDataTable() {
        GridPane gridPane = new GridPane();
        Text helpText = new Text("Asks");
        helpText.setFont(Font.font("Verdana", FontWeight.BOLD, 18));
        helpText.setFill(Color.WHITE);
        helpText.setStroke(Color.web("#7080A0"));
        gridPane.addRow(1, helpText);
        gridPane.addRow(2, asksTableView);
        gridPane.setPadding(new Insets(8));
        gridPane.setStyle("-fx-background-color: gray");
        asksTableView.setItems(controller.getAsks());
        asksTableView.getColumns().setAll(
                TableColumnBuilder.<Ask, String>create()
                        .text("Rate")
                        .cellValueFactory(new PropertyValueFactory<Ask, String>("r"))
                        .prefWidth(100)
                        .build(),
                TableColumnBuilder.<Ask, String>create()
                        .text("Amount")
                        .cellValueFactory(new PropertyValueFactory<Ask, String>("a"))
                        .prefWidth(100)
                        .build(),
                TableColumnBuilder.<Ask, String>create()
                        .text("Value")
                        .cellValueFactory(new PropertyValueFactory<Ask, String>("v"))
                        .prefWidth(100)
                        .build(),
                TableColumnBuilder.<Ask, String>create()
                        .text("Type")
                        .cellValueFactory(new PropertyValueFactory<Ask, String>("t"))
                        .prefWidth(100)
                        .build(),
                TableColumnBuilder.<Ask, String>create()
                        .text("TimeStamp")
                        .cellValueFactory(new PropertyValueFactory<Ask, String>("d"))
                        .prefWidth(100)
                        .build()

        );
        asksTableView.setPrefHeight(250);
        return gridPane;
    }

    @SuppressWarnings("unchecked")
    private Node createBidsOrderBookDataTable() {
        GridPane gridPane = new GridPane();
        Text helpText = new Text("Full BidsOrderBook");
        helpText.setFont(Font.font("Verdana", FontWeight.BOLD, 18));
        helpText.setFill(Color.WHITE);
        helpText.setStroke(Color.web("#7080A0"));
        gridPane.addRow(1, helpText);
        gridPane.addRow(2, bidsOrderBookTableView);
        gridPane.setPadding(new Insets(8));
        gridPane.setStyle("-fx-background-color: gray");
        bidsOrderBookTableView.setItems(controller.getBidsOrderBook());
        bidsOrderBookTableView.getColumns().setAll(
                TableColumnBuilder.<DiffOrderBook, String>create()
                        .text("Book")
                        .cellValueFactory(new PropertyValueFactory<DiffOrderBook, String>("book"))
                        .prefWidth(100)
                        .build(),
                TableColumnBuilder.<DiffOrderBook, BigDecimal>create()
                        .text("Amount")
                        .cellValueFactory(new PropertyValueFactory<DiffOrderBook, BigDecimal>("amount"))
                        .prefWidth(100)
                        .build(),
                TableColumnBuilder.<DiffOrderBook, BigDecimal>create()
                        .text("Price")
                        .cellValueFactory(new PropertyValueFactory<DiffOrderBook, BigDecimal>("price"))
                        .prefWidth(100)
                        .build(),
                TableColumnBuilder.<DiffOrderBook, String>create()
                        .text("Order Id")
                        .cellValueFactory(new PropertyValueFactory<DiffOrderBook, String>("oid"))
                        .prefWidth(100)
                        .build()

        );
        bidsOrderBookTableView.setPrefHeight(250);
        return gridPane;
    }


    @SuppressWarnings("unchecked")
    private Node createAsksOrderBookDataTable() {
        GridPane gridPane = new GridPane();
        Text helpText = new Text("Full AskOrderBook");
        helpText.setFont(Font.font("Verdana", FontWeight.BOLD, 18));
        helpText.setFill(Color.WHITE);
        helpText.setStroke(Color.web("#7080A0"));
        gridPane.addRow(1, helpText);
        gridPane.addRow(2, asksOrderBookTableView);
        gridPane.setPadding(new Insets(8));
        gridPane.setStyle("-fx-background-color: gray");
        asksOrderBookTableView.setItems(controller.getAsksOrderBook());
        asksOrderBookTableView.getColumns().setAll(
                TableColumnBuilder.<DiffOrderBook, String>create()
                        .text("Book")
                        .cellValueFactory(new PropertyValueFactory<DiffOrderBook, String>("book"))
                        .prefWidth(100)
                        .build(),
                TableColumnBuilder.<DiffOrderBook, BigDecimal>create()
                        .text("Amount")
                        .cellValueFactory(new PropertyValueFactory<DiffOrderBook, BigDecimal>("amount"))
                        .prefWidth(100)
                        .build(),
                TableColumnBuilder.<DiffOrderBook, BigDecimal>create()
                        .text("Price")
                        .cellValueFactory(new PropertyValueFactory<DiffOrderBook, BigDecimal>("price"))
                        .prefWidth(100)
                        .build(),
                TableColumnBuilder.<DiffOrderBook, String>create()
                        .text("Order Id")
                        .cellValueFactory(new PropertyValueFactory<DiffOrderBook, String>("oid"))
                        .prefWidth(100)
                        .build()

        );
        asksOrderBookTableView.setPrefHeight(250);
        return gridPane;
    }

    @SuppressWarnings("unchecked")
    private Node createmaxAndMinsDataTable() {
        GridPane gridPane = new GridPane();
        Text helpText = new Text("FullOrderBook Max and Min");
        helpText.setFont(Font.font("Verdana", FontWeight.BOLD, 18));
        helpText.setFill(Color.WHITE);
        helpText.setStroke(Color.web("#7080A0"));
        gridPane.addRow(1, helpText);
        gridPane.addRow(2, maxAndMinDataTableView);
        gridPane.setPadding(new Insets(8));
        gridPane.setStyle("-fx-background-color: gray");
        maxAndMinDataTableView.setItems(this.controller.getLastOrderBooks());
        maxAndMinDataTableView.getColumns().setAll(
                TableColumnBuilder.<LastOrderBook, Integer>create()
                        .text("Secuence")
                        .cellValueFactory(new PropertyValueFactory<LastOrderBook, Integer>("secuence"))
                        .prefWidth(100)
                        .build(),
                TableColumnBuilder.<LastOrderBook, BigDecimal>create()
                        .text("Bids Max Price")
                        .cellValueFactory(new PropertyValueFactory<LastOrderBook, BigDecimal>("bidsMaxPrice"))
                        .prefWidth(100)
                        .build(),
                TableColumnBuilder.<LastOrderBook, BigDecimal>create()
                        .text("Bids Min Price")
                        .cellValueFactory(new PropertyValueFactory<LastOrderBook, BigDecimal>("bidsMinPrice"))
                        .prefWidth(100)
                        .build(),
                TableColumnBuilder.<LastOrderBook, BigDecimal>create()
                        .text("Asks Max Price")
                        .cellValueFactory(new PropertyValueFactory<LastOrderBook, BigDecimal>("asksMaxPrice"))
                        .prefWidth(100)
                        .build(),
                TableColumnBuilder.<LastOrderBook, BigDecimal>create()
                        .text("Asks Min Price")
                        .cellValueFactory(new PropertyValueFactory<LastOrderBook, BigDecimal>("asksMinPrice"))
                        .prefWidth(100)
                        .build()

        );
        maxAndMinDataTableView.setPrefHeight(150);
        return gridPane;
    }


    @SuppressWarnings("unchecked")
    private Node createRecentTradesDataTable() {
        GridPane gridPane = new GridPane();
        Text helpText = new Text("Trade");
        helpText.setFont(Font.font("Verdana", FontWeight.BOLD, 18));
        helpText.setFill(Color.WHITE);
        helpText.setStroke(Color.web("#7080A0"));
        gridPane.addRow(1, helpText);
        gridPane.addRow(2, recenttTradesTableView);
        gridPane.setPadding(new Insets(8));
        gridPane.setStyle("-fx-background-color: gray");
        recenttTradesTableView.setItems(controller.getTrades());
        recenttTradesTableView.getColumns().setAll(
                TableColumnBuilder.<Trade, String>create()
                        .text("Book")
                        .cellValueFactory(new PropertyValueFactory<Trade, String>("book"))
                        .prefWidth(100)
                        .build(),
                TableColumnBuilder.<Trade, String>create()
                        .text("Created at")
                        .cellValueFactory(new PropertyValueFactory<Trade, String>("created_at"))
                        .prefWidth(150)
                        .build(),
                TableColumnBuilder.<Trade, String>create()
                        .text("Amount")
                        .cellValueFactory(new PropertyValueFactory<Trade, String>("amount"))
                        .prefWidth(100)
                        .build(),
                TableColumnBuilder.<Trade, String>create()
                        .text("Maker Side")
                        .cellValueFactory(new PropertyValueFactory<Trade, String>("maker_side"))
                        .prefWidth(100)
                        .build(),
                TableColumnBuilder.<Trade, String>create()
                        .text("Price")
                        .cellValueFactory(new PropertyValueFactory<Trade, String>("price"))
                        .prefWidth(100)
                        .build(),
                TableColumnBuilder.<Trade, String>create()
                        .text("Trade ID")
                        .cellValueFactory(new PropertyValueFactory<Trade, String>("tid"))
                        .prefWidth(100)
                        .build()


        );
        recenttTradesTableView.setPrefHeight(250);
        return gridPane;
    }


    private Node createImaginaryTradesDataTable() {
        GridPane gridPane = new GridPane();
        Text helpText = new Text("Imaginary Trades");
        helpText.setFont(Font.font("Verdana", FontWeight.BOLD, 18));
        helpText.setFill(Color.WHITE);
        helpText.setStroke(Color.web("#7080A0"));
        gridPane.addRow(1, helpText);
        gridPane.addRow(2, imaginaryTradesTableView);
        gridPane.setPadding(new Insets(8));
        gridPane.setStyle("-fx-background-color: gray");
        imaginaryTradesTableView.setItems(controller.getImaginaryTrades());
        imaginaryTradesTableView.getColumns().setAll(
                TableColumnBuilder.<Trade, String>create()
                        .text("Book")
                        .cellValueFactory(new PropertyValueFactory<Trade, String>("book"))
                        .prefWidth(100)
                        .build(),
                TableColumnBuilder.<Trade, String>create()
                        .text("Created at")
                        .cellValueFactory(new PropertyValueFactory<Trade, String>("created_at"))
                        .prefWidth(150)
                        .build(),
                TableColumnBuilder.<Trade, String>create()
                        .text("Amount")
                        .cellValueFactory(new PropertyValueFactory<Trade, String>("amount"))
                        .prefWidth(100)
                        .build(),
                TableColumnBuilder.<Trade, String>create()
                        .text("Maker Side")
                        .cellValueFactory(new PropertyValueFactory<Trade, String>("maker_side"))
                        .prefWidth(100)
                        .build(),
                TableColumnBuilder.<Trade, String>create()
                        .text("Price")
                        .cellValueFactory(new PropertyValueFactory<Trade, String>("price"))
                        .prefWidth(100)
                        .build(),
                TableColumnBuilder.<Trade, String>create()
                        .text("Trade ID")
                        .cellValueFactory(new PropertyValueFactory<Trade, String>("tid"))
                        .prefWidth(100)
                        .build()


        );
        imaginaryTradesTableView.setPrefHeight(250);
        return gridPane;
    }



}
