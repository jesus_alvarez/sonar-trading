# README #


### What is this repository for? ###

* This is intended to be a full-stack based enterprise application using Spring Framework, Spring Security, Java FX , REST API and WebSockets. 
* Version 1.0-SNAPSHOT
* [Sonar Trading](https://bitbucket.org/jesus_alvarez/sonar-trading)

### How do I get set up? ###

For run the Sonar Trading App, only need to set JAVA_HOME environment variable and add the executable maven environment to the system PATH.
#####Configuration
* For Windows:
    set JAVA_HOME=C:\Program Files\Java\jdk1.8.0_144\.
    
    set PATH=C:\java\apache-maven-3.0.5\bin\;%PATH%\.
    
    Set your own preferences.
    
* For Linux :
    export JAVA_HOME=/usr/lib/java/jdk1.8.0_144/.
    
    export PATH=/usr/lib/java/apache-maven-3.0.5/bin:$PATH.
    
    Set your ouwn preferences. 
    
#####Update Java FX runtime 
* Run maven goal below:
    mvn com.zenjava:javafx-maven-plugin:2.0:fix-classpath.
    
    Or
    
    Configure manually the path to jfxrt.jar, there's a example in pom.xml

* Finally run maven goal:
    mvn jfx:run   
    
* This Version was tested only for Maven 3.0.5
* The connection through proxy configuration settings only works for REST API, I'll work for websockets later.


### For any question please contact me at: ###
* Jose de Jesus Alvarez Buendia
* jesuslvrz28@gmail.com
