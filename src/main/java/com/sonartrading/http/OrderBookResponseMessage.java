package com.sonartrading.http;

public class OrderBookResponseMessage {

    private String success;
    private OrderBookPayload payload;


    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }


    public OrderBookPayload getPayload() {
        return payload;
    }

    public void setPayload(OrderBookPayload payload) {
        this.payload = payload;
    }


}
