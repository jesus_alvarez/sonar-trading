package com.sonartrading.business;


import com.sonartrading.ws.Trade;

/**
 * Created by C.JALVBU on 10/24/2017.
 */
public interface ContrarianTradingStrategyListener {

    void onSellUptickBTC(Trade trade);
    void onBuyDownTickBTC(Trade trade);
}
