package com.sonartrading.ws;

public class Ask {

    private String r;
    private String a;
    private String v;
    private String t;
    private String d;

    public String getR() {
        return r;
    }

    public void setR(String r) {
        this.r = r;
    }

    public String getA() {
        return a;
    }

    public void setA(String a) {
        this.a = a;
    }

    public String getV() {
        return v;
    }

    public void setV(String v) {
        this.v = v;
    }

    public String getT() {
        return t;
    }

    public void setT(String t) {
        this.t = t;
    }

    public String getD() {
        return d;
    }

    public void setD(String d) {
        this.d = d;
    }
}
