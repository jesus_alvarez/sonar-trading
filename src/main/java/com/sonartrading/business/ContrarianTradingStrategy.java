package com.sonartrading.business;

import com.sonartrading.ws.Trade;

/**
 * Created by C.JALVBU on 10/24/2017.
 */
public class ContrarianTradingStrategy {

    private Trade lastTrade;
    private Trade lastZeroTick;

    private Integer N;
    private Integer M;

    private int uptick;
    private int zeroTick;
    private int downTick;

    ContrarianTradingStrategyListener listener;

    public ContrarianTradingStrategy(Integer n, Integer m, ContrarianTradingStrategyListener listener) {
        N = n;
        M = m;
        this.listener = listener;
    }

    public void executeTrade(Trade trade){
            if (lastTrade==null)
                lastTrade = trade;
            if (trade.getV().compareTo(lastTrade.getV()) == 0){
               //zerotick
               zeroTick++;
               lastZeroTick = trade;
            }else if(trade.getV().compareTo(lastZeroTick.getV()) == 1){
               // uptick
               uptick++;
               lastTrade = trade;
               checkUpTicks(trade);
            }else if(trade.getV().compareTo(lastZeroTick.getV()) <0){
               //downtick
               downTick++;
               lastTrade=trade;
               checkDownTicks(trade);
            }
    }

    private void checkDownTicks(Trade trade) {
        if (downTick== N.intValue()) {
            if (listener != null) {
                listener.onBuyDownTickBTC(trade);
            }
            downTick=0;
        }
    }

    private void checkUpTicks(Trade trade) {
        if (uptick==M.intValue()) {
            if (listener != null) {
                listener.onSellUptickBTC(trade);
            }
            uptick=0;
        }
    }

    public Integer getN() {
        return N;
    }

    public void setN(Integer n) {
        N = n;
    }

    public Integer getM() {
        return M;
    }

    public void setM(Integer m) {
        M = m;
    }
}
