package com.sonartrading.http;

import java.util.List;

public class OrderBookPayload {

    private String updated_at;
    private Integer sequence;
    private List<DiffOrderBook> bids;
    private List<DiffOrderBook> asks;


    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public Integer getSequence() {
        return sequence;
    }

    public void setSequence(Integer sequence) {
        this.sequence = sequence;
    }

    public List<DiffOrderBook> getBids() {
        return bids;
    }

    public void setBids(List<DiffOrderBook> bids) {
        this.bids = bids;
    }

    public List<DiffOrderBook> getAsks() {
        return asks;
    }

    public void setAsks(List<DiffOrderBook> asks) {
        this.asks = asks;
    }



}
