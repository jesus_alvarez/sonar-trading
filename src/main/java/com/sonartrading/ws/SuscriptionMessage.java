package com.sonartrading.ws;

public class SuscriptionMessage {

    private String action;
    private String book;

    public SuscriptionMessage(String book) {
        this.action = "subscribe";
        this.book = book;
    }


    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getBook() {
        return book;
    }

    public void setBook(String book) {
        this.book = book;
    }
}
