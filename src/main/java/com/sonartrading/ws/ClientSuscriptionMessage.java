package com.sonartrading.ws;

public class ClientSuscriptionMessage extends SuscriptionMessage{

    private String type;

    public ClientSuscriptionMessage(String book, String type) {
        super(book);
        this.type = type;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
