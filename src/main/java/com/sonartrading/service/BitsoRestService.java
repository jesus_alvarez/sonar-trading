package com.sonartrading.service;

import com.sonartrading.http.BitsoHttpGateway;
import com.sonartrading.http.OrderBookResponseMessage;
import com.sonartrading.http.ProxySettings;
import com.sonartrading.http.Trade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by C.JALVBU on 10/17/2017.
 */
@Repository
public class BitsoRestService {

    @Autowired
    BitsoHttpGateway bitsoHttpGateway;

    public List<Trade> getTrades(String book, Integer limit,String sort, boolean byProxy, ProxySettings proxySettings) throws Exception{
        return bitsoHttpGateway.getTrades(book,limit,sort, byProxy,proxySettings);
    }

    public OrderBook getOrderBook(String book,boolean aggregate, boolean byProxy,ProxySettings proxySettings) throws Exception{
        OrderBookResponseMessage responseMessage = bitsoHttpGateway.getOrderBooks(book,aggregate, byProxy,proxySettings);
        return new OrderBook(responseMessage.getPayload().getBids(), responseMessage.getPayload().getAsks(), responseMessage.getPayload().getSequence());
    }
}
