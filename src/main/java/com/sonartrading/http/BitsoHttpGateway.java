package com.sonartrading.http;

import org.apache.http.HttpHost;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.HttpClient;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustStrategy;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import javax.net.ssl.SSLContext;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

@Component
public class BitsoHttpGateway {

    private String host;

    @Autowired
    RestTemplate restTemplate;

    private static final Logger LOGGER = Logger.getLogger(BitsoHttpGateway.class.getName());




    public List<Trade> getTrades(String  book, Integer limit,String sort, boolean byProxy, ProxySettings proxySettings) throws Exception{
        TradeHttpResponseMessage  response = restTemplate(byProxy, proxySettings).getForObject(getTradesUrl(book, limit, sort), TradeHttpResponseMessage.class);
        return response.getPayload();
    }

    public OrderBookResponseMessage getOrderBooks(String book,boolean aggregate, boolean byProxy, ProxySettings proxySettings) throws Exception{
        OrderBookResponseMessage  response = restTemplate(byProxy, proxySettings).getForObject(getOrderBooksUrl(book, aggregate), OrderBookResponseMessage.class);
        return response;
    }

    private RestTemplate restTemplate(boolean byProxy, ProxySettings proxySettings){
        RestTemplate restTemplate;
        if (byProxy){
            restTemplate = restTemplateThroughProxy(proxySettings);
        }else{
            restTemplate = restTemplate();
        }
        return  restTemplate;
    }

    private String getOrderBooksUrl(String  book, boolean aggregate){
        return getHostUrl() + "order_book/?book="+ book + "&aggregate=" + aggregate;
    }

    private String getTradesUrl(String  book, Integer limit, String sort){
        return getHostUrl() + "trades/?book="+ book + "&limit=" + limit + "&sort="+ sort;
    }

    private String  getHostUrl(){
        String hostUrl = host;
        return hostUrl;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public RestTemplate restTemplate() {
        TrustStrategy acceptingTrustStrategy = (X509Certificate[] chain, String authType) -> true;

        SSLContext sslContext = null;
        try {
            sslContext = org.apache.http.conn.ssl.SSLContexts.custom()
                    .loadTrustMaterial(null, acceptingTrustStrategy)
                    .build();
        } catch (NoSuchAlgorithmException e) {
           LOGGER.log(Level.SEVERE, "Error", e);
        } catch (KeyManagementException e) {
            LOGGER.log(Level.SEVERE, "Error", e);
        } catch (KeyStoreException e) {
            LOGGER.log(Level.SEVERE, "Error", e);
        }

        SSLConnectionSocketFactory csf = new SSLConnectionSocketFactory(sslContext);

        CloseableHttpClient httpClient = HttpClients.custom()
                .setSSLSocketFactory(csf)
                .build();

        HttpComponentsClientHttpRequestFactory requestFactory =
                new HttpComponentsClientHttpRequestFactory();

        requestFactory.setHttpClient(httpClient);
        restTemplate.setRequestFactory(requestFactory);
        return restTemplate;
    }

    public RestTemplate restTemplateThroughProxy(ProxySettings proxySettings){
        final String username = proxySettings.getProxyUser();
        final String password = proxySettings.getProxyPassword();
        final String proxyUrl = proxySettings.getProxyHost();
        final int port = proxySettings.getProxyPort();

        CredentialsProvider credsProvider = new BasicCredentialsProvider();
        credsProvider.setCredentials(
                new AuthScope(proxyUrl, port),
                new UsernamePasswordCredentials(username, password));

        HttpHost myProxy = new HttpHost(proxyUrl, port);
        HttpClientBuilder clientBuilder = HttpClientBuilder.create();

        clientBuilder.setProxy(myProxy).setDefaultCredentialsProvider(credsProvider).disableCookieManagement();

        HttpClient httpClient = clientBuilder.build();
        HttpComponentsClientHttpRequestFactory factory = new HttpComponentsClientHttpRequestFactory();
        factory.setHttpClient(httpClient);
        restTemplate.setRequestFactory(factory);
        return restTemplate;
    }

}
