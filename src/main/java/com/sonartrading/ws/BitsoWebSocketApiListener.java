package com.sonartrading.ws;


import java.util.List;

public interface BitsoWebSocketApiListener {
    void onBidReceived(List<Bid> bids);
    void onAskReceived(List<Ask> asks);
    void onDiffOrderReceived(DiffOrder diffOrder);
    void onTradeReceived(List<Trade> trades);
    void onConnect();
}
