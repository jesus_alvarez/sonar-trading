package com.sonartrading;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.stage.Stage;

import java.util.logging.Logger;


/**
 * Created by C.JALVBU on 10/13/2017.
 */
public class Main extends Application{

    private TableView bidsTable = new TableView();
    private TableView asksTable = new TableView();
    private TableView tradesTable = new TableView();
    TextField textFieldX = new TextField ();

    private Integer xValue;


    public static final Logger LOGGER = Logger.getLogger(Main.class.getName());


    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage stage) throws Exception {

        Label xLabel = new Label("X:");

        HBox hbX = new HBox();
        hbX.getChildren().addAll(xLabel, textFieldX);
        hbX.setSpacing(10);

        textFieldX.textProperty().addListener((observable, oldValue, newValue) -> {
            LOGGER.info("textFieldX changed from " + oldValue + " to " + newValue);
            xValue = Integer.valueOf(newValue);
        });

        Scene scene = new Scene(new Group());
        stage.setTitle("Table View Sample");
        stage.setWidth(1100);
        stage.setHeight(700);

        final Label orderBookslabel = new Label("Order Books");
        orderBookslabel.setFont(new Font("Arial", 20));

        final Label tradesLabel = new Label("Trade");
        tradesLabel.setFont(new Font("Arial", 20));

        createAsksAndBidsTable();
        createTradesTable();

        final Label bidsLabel = new Label("Bid");
        bidsLabel.setFont(new Font("Arial", 15));

        final Label askslabel = new Label("Ask");
        askslabel.setFont(new Font("Arial", 15));


        bidsTable.setMaxHeight(250);
        asksTable.setMaxHeight(250);
        tradesTable.setMaxHeight(250);

        final VBox vbox = new VBox();
        vbox.setSpacing(5);
        vbox.setPadding(new Insets(10, 0, 0, 10));

        final VBox vbox2 = new VBox();
        vbox2.setSpacing(5);
        vbox2.setPadding(new Insets(60, 0, 0, 10));

        vbox.getChildren().addAll(hbX, orderBookslabel,bidsLabel, bidsTable,askslabel ,asksTable);
        vbox2.getChildren().addAll(tradesLabel, tradesTable);

        HBox hbX2 = new HBox();
        hbX2.getChildren().addAll(vbox, vbox2);
        hbX2.setSpacing(10);

        ((Group) scene.getRoot()).getChildren().addAll(hbX2);

        stage.setScene(scene);
        stage.show();
    }

    private void createTradesTable() {

        TableColumn uniqueIDCol = new TableColumn("Unique ID");
        TableColumn amounColt = new TableColumn("Amount");
        TableColumn rateCol = new TableColumn("Rate");
        TableColumn valueCol = new TableColumn("Value");
        TableColumn typeCol = new TableColumn("Type");
        TableColumn moCol = new TableColumn("MO ID");
        TableColumn toCol = new TableColumn("TO ID");

        tradesTable.getColumns().addAll(uniqueIDCol,rateCol, amounColt, valueCol, typeCol, moCol, toCol);
    }


    private void createAsksAndBidsTable() {

        TableColumn rateCol = new TableColumn("Rate");
        TableColumn amounColt = new TableColumn("Amount");
        TableColumn valueCol = new TableColumn("Value");
        TableColumn typeCol = new TableColumn("Type");
        TableColumn timestampCol = new TableColumn("Timestamp");

        asksTable.getColumns().addAll(rateCol, amounColt, valueCol, typeCol, timestampCol);
        bidsTable.getColumns().addAll(rateCol, amounColt, valueCol, typeCol, timestampCol);
    }
}
