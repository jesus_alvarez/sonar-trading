package com.sonartrading.ws;

import java.math.BigDecimal;

public class Trade {

    private Integer i;
    private String a;
    private String r;
    private BigDecimal v;
    private Integer t;
    private String mo;
    private String to;

    public Integer getI() {
        return i;
    }

    public void setI(Integer i) {
        this.i = i;
    }

    public String getA() {
        return a;
    }

    public void setA(String a) {
        this.a = a;
    }

    public String getR() {
        return r;
    }

    public void setR(String r) {
        this.r = r;
    }

    public BigDecimal getV() {
        return v;
    }

    public void setV(BigDecimal v) {
        this.v = v;
    }

    public Integer getT() {
        return t;
    }

    public void setT(Integer m) {
        this.t = t;
    }

    public String getMo() {
        return mo;
    }

    public void setMo(String mo) {
        this.mo = mo;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }
}
