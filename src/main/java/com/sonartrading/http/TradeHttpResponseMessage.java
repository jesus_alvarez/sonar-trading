package com.sonartrading.http;

import java.util.List;

public class TradeHttpResponseMessage {

    private String success;
    private List<Trade> payload;

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public List<Trade> getPayload() {
        return payload;
    }

    public void setPayload(List<Trade> payload) {
        this.payload = payload;
    }
}
