package com.sonartrading.ws;

public interface BitsoWebSocketListener {

    void onOpen();
    void onMessage(String message);
    void onClose();
    void onSubscription(ResponseSuscriptionMessage subscriptionMessage);
    void onServerMessage(ServerJsonMessage message);
}
