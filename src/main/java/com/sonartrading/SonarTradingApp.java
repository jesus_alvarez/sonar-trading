package com.sonartrading;

import com.sonartrading.configuration.SonarTradingAppConfiguration;
import com.sonartrading.configuration.ScreensConfiguration;
import javafx.application.Application;
import javafx.stage.Stage;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * Created by C.JALVBU on 10/16/2017.
 */
public class SonarTradingApp extends Application{

        public static void main(String[] args) {
            launch(args);
        }

        @Override
        public void start(Stage stage) throws Exception {
            ApplicationContext context = new AnnotationConfigApplicationContext(SonarTradingAppConfiguration.class);
            ScreensConfiguration screens = context.getBean(ScreensConfiguration.class);
            screens.setPrimaryStage(stage);
            screens.loginDialog().show();
        }

}
