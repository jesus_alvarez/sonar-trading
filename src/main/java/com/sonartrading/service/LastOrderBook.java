package com.sonartrading.service;

import com.sonartrading.http.DiffOrderBook;

import java.math.BigDecimal;
import java.util.*;

public class LastOrderBook {
    private Integer secuence;
    private Map<String, DiffOrderBook> bids  = new HashMap<>();
    private Map<String, DiffOrderBook> asks  = new HashMap<>();

    private BigDecimal bidsMaxPrice;
    private BigDecimal bidsMinPrice;
    private BigDecimal asksMaxPrice;
    private BigDecimal asksMinPrice;

    public Integer getSecuence() {
        return secuence;
    }

    public void setSecuence(Integer secuence) {
        this.secuence = secuence;
    }

    public Map<String, DiffOrderBook> getBids() {
        return bids;
    }

    public void setBids(Map<String, DiffOrderBook> bids) {
        this.bids = bids;
    }

    public Map<String, DiffOrderBook> getAsks() {
        return asks;
    }

    public void setAsks(Map<String, DiffOrderBook> asks) {
        this.asks = asks;
    }

    public BigDecimal getBidsMaxPrice() {
        return bidsMaxPrice;
    }

    public BigDecimal getBidsMinPrice() {
        return bidsMinPrice;
    }

    public BigDecimal getAsksMaxPrice() {
        return asksMaxPrice;
    }

    public BigDecimal getAsksMinPrice() {
        return asksMinPrice;
    }

    public void updateMaxAndMinPrice(){
        ArrayList<BigDecimal> bidsPrices = new ArrayList<BigDecimal>();
        ArrayList<BigDecimal> asksPrices = new ArrayList<BigDecimal>();
        Iterator<Map.Entry<String, DiffOrderBook>> itBids = bids.entrySet().iterator();

        while (itBids.hasNext()){
            Map.Entry<String, DiffOrderBook> entry = itBids.next();
            bidsPrices.add(entry.getValue().getPrice());
        }

        Iterator<Map.Entry<String, DiffOrderBook>> itAks = asks.entrySet().iterator();

        while (itAks.hasNext()){
            Map.Entry<String, DiffOrderBook> entry = itAks.next();
            asksPrices.add(entry.getValue().getPrice());
        }

        Collections.sort(bidsPrices);
        Collections.sort(asksPrices);

        if (bidsPrices.size()>0){
            bidsMaxPrice = bidsPrices.get(bidsPrices.size() -1);
            bidsMinPrice =  bidsPrices.get(0);
        }
        if (asksPrices.size()>0){
            asksMaxPrice =  asksPrices.get(asksPrices.size() -1);
            asksMinPrice = asksPrices.get(0);
        }
    }
}
