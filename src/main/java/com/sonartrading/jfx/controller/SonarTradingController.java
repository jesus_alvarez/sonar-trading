package com.sonartrading.jfx.controller;

import com.sonartrading.business.ContrarianTradingStrategy;
import com.sonartrading.business.ContrarianTradingStrategyListener;
import com.sonartrading.configuration.ScreensConfiguration;
import com.sonartrading.http.DiffOrderBook;
import com.sonartrading.http.Trade;
import com.sonartrading.service.BitsoRestService;
import com.sonartrading.service.LastOrderBook;
import com.sonartrading.service.OrderBook;
import com.sonartrading.utils.NumberUtils;
import com.sonartrading.ws.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

/**
 * Created by C.JALVBU on 10/16/2017.
 */
@Controller
public class SonarTradingController  implements DialogController, BitsoWebSocketApiListener{

    private ScreensConfiguration screens;
    private FXMLDialog dialog;


    @Autowired
    BitsoRestService bitsoRestService;

    @Autowired
    BitsoWebSocketGateway bitsoWebSocketGateway;

    List<DiffOrder> diffMessagesReceived = new ArrayList<>();

    private static final String BTC_MXN_BOOK = "btc_mxn";
    private static final String TRADES_SORT_DESC = "desc";


    public static final Logger LOGGER = Logger.getLogger(SonarTradingController.class.getName());

    private ObservableList<Bid> bids = FXCollections.observableArrayList();
    private ObservableList<Ask> asks = FXCollections.observableArrayList();
    private ObservableList<Trade> trades = FXCollections.observableArrayList();
    private ObservableList<Trade> imaginaryTrades  = FXCollections.observableArrayList();

    LastOrderBook lastOrderBook = new LastOrderBook();

    private  Integer X =  20;
    private  Integer N =  3;
    private  Integer M =  3;
    private boolean byProxy;
    private ObservableList<DiffOrderBook> asksOrderBook = FXCollections.observableArrayList();
    private ObservableList<DiffOrderBook> bidsOrderBook = FXCollections.observableArrayList();
    private ObservableList<LastOrderBook> lastOrderBooks = FXCollections.observableArrayList();

    ContrarianTradingStrategy contrarianTradingStrategy;

    ContrarianTradingStrategyListener contrarianTradingStrategyListener = new MyContrarianTradingStrategy();

    @Override
    public void setDialog(FXMLDialog dialog) {
        this.dialog = dialog;
    }

    public SonarTradingController(ScreensConfiguration screens) {
        this.screens = screens;
    }

    public ObservableList<Bid> getBids(){
        return bids;
    }


    public ObservableList<Ask> getAsks(){
        return asks;
    }

    public ObservableList<Trade> getTrades() {
        if (trades.isEmpty())
            try {
                trades.addAll(bitsoRestService.getTrades(BTC_MXN_BOOK, X,TRADES_SORT_DESC, byProxy, screens.getProxySettings()));
            } catch (Exception e) {
                LOGGER.info("Connection Error : " + e.getMessage());
                screens.errorDialog().show();
            }
        return trades;
    }

    public void connectoToWebSOcket() {
        bitsoWebSocketGateway.connect(this);
    }

    public void initialOrderBook(){
        try {
            OrderBook orderBook = bitsoRestService.getOrderBook(BTC_MXN_BOOK,false, byProxy,screens.getProxySettings());

            for (DiffOrderBook bid : orderBook.getBids()){
                lastOrderBook.getBids().put(bid.getOid(), bid);
            }
            for (DiffOrderBook ask : orderBook.getAsks()){
                lastOrderBook.getAsks().put(ask.getOid(), ask);
            }
            lastOrderBook.setSecuence(orderBook.getSecuence());
            lastOrderBook.updateMaxAndMinPrice();

            updateObservableAsksAndBidsCollections();

        } catch (Exception e) {
            LOGGER.info("Connection Error : " + e.getMessage());
            screens.errorDialog().show();
        }
    }

    private void updateObservableAsksAndBidsCollections() {
        asksOrderBook.clear();
        bidsOrderBook.clear();
        asksOrderBook.addAll(lastOrderBook.getAsks().values());
        bidsOrderBook.addAll(lastOrderBook.getBids().values());
        lastOrderBooks.clear();
        lastOrderBooks.add(lastOrderBook);
    }

    @Override
    public void onBidReceived(List<Bid> bids) {
        this.bids.clear();
        this.bids.addAll(bids.subList(0 , X ));
    }

    @Override
    public void onAskReceived(List<Ask> asks) {
        this.asks.clear();
        this.asks.addAll(asks.subList(0 , X ));
    }

    @Override
    public void onDiffOrderReceived(DiffOrder diffOrder) {

        if (lastOrderBook.getSecuence() == null){
            diffMessagesReceived.add(diffOrder);
            return;
        }else{
            for (DiffOrder order : diffMessagesReceived){
                if (order.getSecuence().compareTo(lastOrderBook.getSecuence()) > 0){
                    mergeOrderBook(diffOrder);
                }else{
                    LOGGER.info("Discarded diff-order secuence " + diffOrder.getSecuence());
                }
            }
            diffMessagesReceived.clear();
        }

        if (diffOrder.getSecuence().compareTo(lastOrderBook.getSecuence()) > 0){
            mergeOrderBook(diffOrder);
        }


    }

    @Override
    public void onTradeReceived(List<com.sonartrading.ws.Trade> trades) {
        if (contrarianTradingStrategy==null) {
            contrarianTradingStrategy = new ContrarianTradingStrategy(N, M, contrarianTradingStrategyListener);
        }
        for (com.sonartrading.ws.Trade trade : trades){
            contrarianTradingStrategy.executeTrade(trade);
        }
    }

    private void mergeOrderBook(DiffOrder diffOrder) {
        lastOrderBook.setSecuence(diffOrder.getSecuence());
        for (DiffOrdersPayLoad dif : diffOrder.getPayLoads()){
            if ("1".equals(dif.getT())){
                if (lastOrderBook.getBids().containsKey(dif.getO())){
                    DiffOrderBook diffOrderBook = lastOrderBook.getBids().get(dif.getO());
                    if (dif.getA()==null){
                        lastOrderBook.getBids().remove(dif.getO());
                    }else{
                        updateDiffOrderBook(diffOrderBook, dif, diffOrder.getBook());
                    }
                }else{
                    if (dif.getA()!=null){
                        DiffOrderBook diffOrderBook = new DiffOrderBook();
                        updateDiffOrderBook(diffOrderBook, dif, diffOrder.getBook());
                        lastOrderBook.getBids().put(dif.getO(), diffOrderBook);
                    }
                }
            }else{
                if (lastOrderBook.getAsks().containsKey(dif.getO())){
                    DiffOrderBook diffOrderBook = lastOrderBook.getAsks().get(dif.getO());
                    if (dif.getA()==null){
                        lastOrderBook.getAsks().remove(dif.getO());
                    }else{
                        updateDiffOrderBook(diffOrderBook, dif, diffOrder.getBook());
                    }
                }else{
                    if (dif.getA()!=null){
                        DiffOrderBook diffOrderBook = new DiffOrderBook();
                        updateDiffOrderBook(diffOrderBook, dif, diffOrder.getBook());
                        lastOrderBook.getAsks().put(dif.getO(), diffOrderBook);
                    }
                }

            }
            lastOrderBook.updateMaxAndMinPrice();
            lastOrderBooks.clear();
            lastOrderBooks.add(lastOrderBook);
            updateObservableAsksAndBidsCollections();
        }
    }

    private void updateDiffOrderBook(DiffOrderBook diffOrderBook, DiffOrdersPayLoad diffOrder, String book) {
        diffOrderBook.setAmount(NumberUtils.toBigDecimal(diffOrder.getA()));
        diffOrderBook.setOid(diffOrder.getO());
        diffOrderBook.setPrice(NumberUtils.toBigDecimal(diffOrder.getV()));
        diffOrderBook.setBook(book);
    }

    @Override
    public void onConnect() {
        bitsoWebSocketGateway.subscribeToDiffOrdersChannel(BTC_MXN_BOOK);
        bitsoWebSocketGateway.subscribeToOrdersChannel(BTC_MXN_BOOK);
        bitsoWebSocketGateway.subscribeToTradesChannel(BTC_MXN_BOOK);
        this.initialOrderBook();
    }

    public Integer getX() {
        return X;
    }

    public void setX(Integer x) {
        X = x;
    }

    public Integer getN() {
        return N;
    }

    public void setN(Integer n) {
        N = n;
        if (contrarianTradingStrategy!=null) {
            contrarianTradingStrategy.setN(N);
        }
    }

    public Integer getM() {
        return M;
    }

    public void setM(Integer m) {
        M = m;
        if (contrarianTradingStrategy!=null) {
            contrarianTradingStrategy.setM(M);
        }
    }

    public void setByProxy(boolean byProxy) {
        this.byProxy = byProxy;
    }

    public boolean isByProxy() {
        return byProxy;
    }

    public ObservableList<DiffOrderBook> getAsksOrderBook() {
        return asksOrderBook;
    }

    public ObservableList<DiffOrderBook> getBidsOrderBook() {
        return bidsOrderBook;
    }

    public ObservableList<LastOrderBook> getLastOrderBooks() {
        return lastOrderBooks;
    }

    private class MyContrarianTradingStrategy implements ContrarianTradingStrategyListener{

        @Override
        public void onSellUptickBTC(com.sonartrading.ws.Trade trade) {
            LOGGER.info("onSellUptickBTC "+ trade.getI());
            Trade t = new Trade();
            t.setAmount(trade.getA());
            t.setMaker_side("Sell");
            t.setPrice(trade.getV().toString());
            t.setTid(trade.getI().longValue());
            t.setCreated_at("SellUptickBTC");
            t.setBook(BTC_MXN_BOOK);
            addTradeToImaginaryTrades(t);
        }

        @Override
        public void onBuyDownTickBTC(com.sonartrading.ws.Trade trade) {
            LOGGER.info("onBuyDownTickBTC "+ trade.getI());
            Trade t = new Trade();
            t.setAmount(trade.getA());
            t.setMaker_side("Buy");
            t.setPrice(trade.getV().toString());
            t.setTid(trade.getI().longValue());
            t.setCreated_at("BuyDownTickBTC");
            t.setBook(BTC_MXN_BOOK);
            addTradeToImaginaryTrades(t);
        }
    }

    private void addTradeToImaginaryTrades(Trade trade){
        imaginaryTrades.add(trade);
    }

    public ObservableList<Trade> getImaginaryTrades() {
        return imaginaryTrades;
    }
}
