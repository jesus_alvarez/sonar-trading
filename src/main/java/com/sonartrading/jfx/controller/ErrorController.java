package com.sonartrading.jfx.controller;

import javafx.fxml.FXML;
import javafx.scene.control.Label;

public class ErrorController implements DialogController {
    private FXMLDialog dialog;
    private String error;

    @FXML
    Label labelError;

    public void setDialog(FXMLDialog dialog) {
        this.dialog = dialog;
    }

    @FXML
    public void close() {
        dialog.close();
    }
}