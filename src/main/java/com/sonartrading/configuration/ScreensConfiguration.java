package com.sonartrading.configuration;

import com.sonartrading.http.ProxySettings;
import com.sonartrading.jfx.controller.*;
import com.sonartrading.screen.SonarTradingScreen;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Scope;

@Configuration
@Lazy
public class ScreensConfiguration {
    private Stage primaryStage;
    private ProxySettings proxySettings;
    private boolean byProxy;

    public void setPrimaryStage(Stage primaryStage) {
        this.primaryStage = primaryStage;
    }

    public void showScreen(Parent screen) {
        primaryStage.setScene(new Scene(screen, 777, 500));
        primaryStage.show();
    }

    public void close(){
        primaryStage.close();
    }



    @Bean
    @Scope("prototype")
    public FXMLDialog errorDialog() {
        return new FXMLDialog(errorController(), getClass().getResource("/fxml/Error.fxml"), primaryStage, StageStyle.UNDECORATED);
    }

    @Bean
    @Scope("prototype")
    ProxyController proxyController() {
        return new ProxyController(this);
    }

    @Bean
    @Scope("prototype")
    public FXMLDialog proxyDialog (){
        return new FXMLDialog(proxyController(), getClass().getResource("/fxml/Proxy.fxml"), primaryStage, StageStyle.UNDECORATED);
    }

    @Bean
    @Scope("prototype")
    ErrorController errorController() {
        return new ErrorController();
    }




    @Bean
    @Scope("prototype")
    public FXMLDialog loginDialog() {
        return new FXMLDialog(loginController(), getClass().getResource("/fxml/Login.fxml"), primaryStage, StageStyle.UNDECORATED);
    }

    @Bean
    @Scope("prototype")
    LoginController loginController() {
        return new LoginController(this);
    }

    @Bean
    @Scope("prototype")
    SonarTradingController sonarTradingController() {
        return new SonarTradingController(this);
    }

    public SonarTradingScreen sonarTradingScreen(){return new SonarTradingScreen(this,sonarTradingController(), byProxy);}

    public ProxySettings getProxySettings() {
        return proxySettings;
    }

    public void setProxySettings(ProxySettings proxySettings) {
        this.proxySettings = proxySettings;
    }

    public boolean getByProxy() {
        return byProxy;
    }

    public void setByProxy(boolean byProxy) {
        this.byProxy = byProxy;
    }
}