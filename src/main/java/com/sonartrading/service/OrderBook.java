package com.sonartrading.service;

import com.sonartrading.http.DiffOrderBook;

import java.util.List;

public class OrderBook {

    private List<DiffOrderBook> bids;
    private List<DiffOrderBook> asks;
    private Integer secuence;

    public OrderBook(List<DiffOrderBook> bids, List<DiffOrderBook> asks, Integer secuence ) {
        this.bids = bids;
        this.asks = asks;
        this.secuence = secuence;
    }

    public OrderBook(Integer secuence) {
        this.secuence = secuence;
    }

    public List<DiffOrderBook> getBids() {
        return bids;
    }

    public void setBids(List<DiffOrderBook> bids) {
        this.bids = bids;
    }

    public List<DiffOrderBook> getAsks() {
        return asks;
    }

    public void setAsks(List<DiffOrderBook> asks) {
        this.asks = asks;
    }

    public Integer getSecuence() {
        return secuence;
    }

    public void setSecuence(Integer secuence) {
        this.secuence = secuence;
    }
}
