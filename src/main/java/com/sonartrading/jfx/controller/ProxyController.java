package com.sonartrading.jfx.controller;

import com.sonartrading.configuration.ScreensConfiguration;
import com.sonartrading.http.ProxySettings;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.TextField;

/**
 * Created by C.JALVBU on 10/20/2017.
 */
public class ProxyController implements DialogController{

    private ScreensConfiguration screens;
    private FXMLDialog dialog;

    @FXML
    TextField proxyHost;
    @FXML
    TextField proxyPort;
    @FXML
    TextField proxyUser;
    @FXML
    TextField proxyPassword;

    @Override
    public void setDialog(FXMLDialog dialog) {
        this.dialog = dialog;
    }

    public ProxyController(ScreensConfiguration screens) {
        this.screens = screens;
    }

    public void saveProxySettings(ActionEvent actionEvent) {
        dialog.close();
        ProxySettings proxySettings = new ProxySettings();
        proxySettings.setProxyHost(proxyHost.getText());
        proxySettings.setProxyPort(Integer.valueOf(proxyPort.getText()));
        proxySettings.setProxyUser(proxyUser.getText());
        proxySettings.setProxyPassword(proxyPassword.getText());
        screens.setProxySettings(proxySettings);
        screens.loginDialog().show();
    }

    public void close(ActionEvent actionEvent) {
        dialog.close();
        screens.loginDialog().show();
    }
}
