package com.sonartrading.ws;

import com.sonartrading.utils.NumberUtils;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.logging.Logger;

@Component
public class BitsoWebSocketGateway {

    private String host;
    private MyBitsoWebSocketlistener listener = new MyBitsoWebSocketlistener();
    private BitsoWebSocketApiListener apiListaner;

    BitsoWebSocketClient client;
    public static final  Logger LOGGER = Logger.getLogger(BitsoWebSocketGateway.class.getName());


    public boolean connect(BitsoWebSocketApiListener apiListaner){
        this.apiListaner = apiListaner;
        client = new BitsoWebSocketClient(host,listener);
        return client.connect();
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public void subscribeToOrdersChannel(String book){
        client.subscribe(book, BitsoChannels.ORDERS.toString());
    }

    public void subscribeToTradesChannel(String book){
        client.subscribe(book, BitsoChannels.TRADES.toString());
    }

    public void subscribeToDiffOrdersChannel(String book){
        client.subscribe(book, BitsoChannels.DIFF_ORDERS.toString());
    }


    private class  MyBitsoWebSocketlistener implements BitsoWebSocketListener{

        @Override
        public void onOpen() {
            if (apiListaner!=null)
                apiListaner.onConnect();
        }

        @Override
        public void onMessage(String message) {
        }

        @Override
        public void onClose() {

        }

        @Override
        public void onSubscription(ResponseSuscriptionMessage subscriptionMessage) {
            LOGGER.info( "Subscription : " + subscriptionMessage.getType() + " " + subscriptionMessage.getResponse() );
        }

        @Override
        public void onServerMessage(ServerJsonMessage message) {
            LOGGER.info("Server Message ; " + message.getType());
            if (BitsoChannels.ORDERS.equals(BitsoChannels.getBitsoChannel(message.getType()))){
                LinkedHashMap payload = (LinkedHashMap) message.getPayload();
                if (payload!=null){
                    List<LinkedHashMap> bids = (List<LinkedHashMap>) payload.get("bids");
                    List<LinkedHashMap> asks = (List<LinkedHashMap>) payload.get("asks");
                    List<Bid> bidsList = new ArrayList<>();
                    List<Ask> asksList = new ArrayList<>();
                    for (LinkedHashMap bid : bids){
                        Bid b = new Bid();
                        b.setR(bid.get("r").toString());
                        b.setA( bid.get("a").toString());
                        b.setV( bid.get("v").toString());
                        b.setT( bid.get("t").toString());
                        b.setD( bid.get("d").toString());
                        bidsList.add(b);
                    }
                    for (LinkedHashMap ask : asks){
                        Ask a = new Ask();
                        a.setR( ask.get("r").toString());
                        a.setA( ask.get("a").toString());
                        a.setV( ask.get("v").toString());
                        a.setT( ask.get("t").toString());
                        a.setD( ask.get("d").toString());
                        asksList.add(a);
                    }
                    apiListaner.onAskReceived(asksList);
                    apiListaner.onBidReceived(bidsList);
                }
            }else if (BitsoChannels.DIFF_ORDERS.equals(BitsoChannels.getBitsoChannel(message.getType()))){
                List<LinkedHashMap> payload = (List<LinkedHashMap>) message.getPayload();
                List<DiffOrdersPayLoad> diffOrderList = new ArrayList<>();
                for (LinkedHashMap diffOrder : payload){
                    DiffOrdersPayLoad dO = new DiffOrdersPayLoad();
                    if (diffOrder.containsKey("a"))
                        dO.setA(diffOrder.get("a").toString());
                    if (diffOrder.containsKey("v"))
                        dO.setV(diffOrder.get("v").toString());

                    dO.setS(diffOrder.get("s").toString());
                    dO.setO(diffOrder.get("o").toString());
                    dO.setD(diffOrder.get("d").toString());
                    dO.setR(diffOrder.get("r").toString());
                    dO.setT(diffOrder.get("t").toString());


                    diffOrderList.add(dO);
                }
                DiffOrder diffOrder = new DiffOrder();
                diffOrder.setBook(message.getBook());
                diffOrder.setPayLoads(diffOrderList);
                diffOrder.setSecuence(Integer.valueOf(message.getSequence()));
                apiListaner.onDiffOrderReceived(diffOrder);
            }else if (BitsoChannels.TRADES.equals(BitsoChannels.getBitsoChannel(message.getType()))){
                List<LinkedHashMap> payload = (List<LinkedHashMap>) message.getPayload();
                List<Trade> tradesList = new ArrayList<>();
                for (LinkedHashMap tradePayLoad : payload){
                    Trade trade  = new Trade();
                    trade.setI(NumberUtils.string2Integer(tradePayLoad.get("i").toString()));
                    trade.setA(tradePayLoad.get("a").toString());
                    trade.setR(tradePayLoad.get("r").toString());
                    trade.setV(NumberUtils.toBigDecimal(tradePayLoad.get("v").toString()));
                    trade.setT(NumberUtils.string2Integer(tradePayLoad.get("t").toString()));
                    trade.setTo(tradePayLoad.get("to").toString());
                    trade.setMo(tradePayLoad.get("mo").toString());
                    tradesList.add(trade);
                }
                apiListaner.onTradeReceived(tradesList);

            }
        }
    }
}
