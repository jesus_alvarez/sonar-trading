package com.sonartrading.utils;

import org.apache.commons.validator.GenericValidator;

import java.math.BigDecimal;

public  class NumberUtils {

    public static BigDecimal toBigDecimal(String number) {
        BigDecimal resultado = null;
        if (StringUtils.checkValidString(number)) {
            resultado = new BigDecimal(number);
        }
        return resultado;
    }

    public static Integer string2Integer(String valor) {
        if(StringUtils.checkValidString(valor) && GenericValidator.isInt(valor)) {
            return  new Integer(Integer.parseInt(valor));
        }
        return null;
    }
}
